package de.Barschnacken.Darragesh.Geld;

import org.bukkit.entity.Player;

import de.Barschnacken.Darragesh.Main;
import de.Barschnacken.Darragesh.Database.Playerdata;
import de.Barschnacken.Darragesh.Database.scoreBoardStats;

public class moneyUtil {
	
	public static void addMoney(String target_uuid, long gold, Player scoreboardPlayer){
		Playerdata targetData = Main.playerdataHandler.spielerDaten.get(target_uuid);
		String goldOld = Long.toString(targetData.getGoldAmount());
		
		targetData.setGoldAmount(targetData.getGoldAmount() + gold);
		String goldNew = Long.toString(targetData.getGoldAmount());
		
		scoreBoardStats.updateGold(scoreboardPlayer, goldOld, goldNew);
	}
	
	public static void removeMoney(String target_uuid, long gold, Player scoreboardPlayer){
		Playerdata targetData = Main.playerdataHandler.spielerDaten.get(target_uuid);
		String goldOld = Long.toString(targetData.getGoldAmount());
		
		targetData.setGoldAmount(targetData.getGoldAmount() - gold);
		String goldNew = Long.toString(targetData.getGoldAmount());
		
		scoreBoardStats.updateGold(scoreboardPlayer, goldOld, goldNew);
	}
	
	public static long getGold(String target_uuid){
		Playerdata targetData = Main.playerdataHandler.spielerDaten.get(target_uuid);
		
		if(targetData != null){
			return targetData.getGoldAmount();
		}
		
		return 0;
	}
	
	public static int getSilber(String target_uuid){
		Playerdata targetData = Main.playerdataHandler.spielerDaten.get(target_uuid);
		
		if(targetData != null){
			return targetData.getSilberAmount();
		}
		
		return 0;
	}
	
	public static boolean checkIfEnough(String target_uuid, long gold, int silber){
		Boolean hasEnough = true;
		
		Playerdata targetData = Main.playerdataHandler.spielerDaten.get(target_uuid);
		
		if(targetData == null){
			return false;
		}
		
		if(targetData.getGoldAmount() < gold || targetData.getSilberAmount() < silber){
			hasEnough = false;
		}
		
		return hasEnough;
	}
	
}
