package de.Barschnacken.Darragesh.Geld;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class payCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {

		if(!(cs instanceof Player)){
			cs.sendMessage("�8[�7Darragesh-System�8]�c Sorry, dieser Befehl ist nur f�r Spieler!");
			return true;
		}
		
		if(!cs.hasPermission("darragesh.player")){
			cs.sendMessage("�8[�7Darragesh�8]�c Du hast nicht die n�tige Berechtigung!");
			return true;
		}
		
		if(args.length < 2){
			cs.sendMessage("�8[�7Darragesh�8]�c Benutze /pay <Spieler> <Gold>");
			return true;
		}
		
		Player targetPlayer = Bukkit.getPlayer(args[0]);
		Player senderPlayer = (Player) cs;
		
		if(targetPlayer == null){
			cs.sendMessage("�8[�7Darragesh�8]�c Der angegebene Spieler ist nicht online.");
			return true;
		}

//		if(Integer.parseInt(args[2]) > 99 || Integer.parseInt(args[2]) < 0){
//			cs.sendMessage("�8[�7Darragesh�8]�c Die Anzahl der zu �berweisenden Silberm�nzen muss zwischen 1 und 99 liegen.");
//			return true;
//		}
		
		if(Long.parseLong(args[1]) < 0){
			cs.sendMessage("�8[�7Darragesh�8]�c Du kannst keine negativen Goldbetr�ge �berweisen.");
			return true;
		}
		
		if(!moneyUtil.checkIfEnough(senderPlayer.getUniqueId().toString(), Long.parseLong(args[1]), 0)){
			cs.sendMessage("�8[�7Darragesh�8]�c Du hast nicht gen�gend Geld.");
			return true;
		}
		
		moneyUtil.removeMoney(senderPlayer.getUniqueId().toString(), Long.parseLong(args[1]), senderPlayer);
		moneyUtil.addMoney(targetPlayer.getUniqueId().toString(), Long.parseLong(args[1]), targetPlayer);
		
		//scoreBoardStats.updateScoreboard(senderPlayer);
		//scoreBoardStats.updateScoreboard(targetPlayer);
		
		cs.sendMessage("�8[�7Darragesh�8]�2 Du hast �6" + targetPlayer.getDisplayName() + "�6 " + args[1] + " Gold �2�berwiesen.");
		return true;
	}

}
