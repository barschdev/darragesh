package de.Barschnacken.Darragesh.ServerUtils;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;

public class antilagRunnable implements Runnable {

	@Override
	public void run() {
		
		Bukkit.broadcastMessage("§8[§7Darragesh§8] Herumliegende Items werden entfernt und unbenutze Chunks werden entladen...");
		
		for(World w : Bukkit.getWorlds()){
			w.save();
			
			for(Entity e : w.getEntities()){
				if(e.getType() == EntityType.DROPPED_ITEM){
					e.remove();
				}
			}
			
			/*for(Chunk c : w.getLoadedChunks()){
				boolean unused = true;
				
				for(Entity e : c.getEntities()){
					if(e.getType() == EntityType.PLAYER){
						unused = false;
					}
				}
				
				if(unused){
					c.unload();
				}
			}*/
		}
	}

}
