package de.Barschnacken.Darragesh.Database;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import de.Barschnacken.Darragesh.Main;
import de.Barschnacken.Darragesh.Geld.moneyUtil;

public class scoreBoardStats implements Listener {

	Main plugin;
	
	public scoreBoardStats(Main plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		final Player p = e.getPlayer();
		scoreBoardStats.setupScoreboard(p);
		
//		Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
//			@Override
//			public void run() {
//				setupScoreboard(p);
//			}
//		}, 100, 100);
	}
	
	@SuppressWarnings("deprecation")
	public static void setupScoreboard(Player p){
		Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
		Objective objective = board.registerNewObjective("stats", "bbb");
		
		objective.setDisplayName("�7=Statistiken=");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
		
		Score name = objective.getScore(Bukkit.getOfflinePlayer("�6Name:"));
		Score nameValue = objective.getScore(Bukkit.getOfflinePlayer(p.getName()));
		
		Score leer1 = objective.getScore(" ");
		
		Score gold = objective.getScore(Bukkit.getOfflinePlayer("�6Gold:"));
		Score goldValue = objective.getScore(Bukkit.getOfflinePlayer(Long.toString(moneyUtil.getGold(p.getUniqueId().toString()))));
		
		Score trenner = objective.getScore(Bukkit.getOfflinePlayer("�7============"));
		
		Score level = objective.getScore(Bukkit.getOfflinePlayer("�6Level:"));
		Score levelValue = objective.getScore(Bukkit.getOfflinePlayer(Integer.toString(Main.playerdataHandler.spielerDaten.get(p.getUniqueId().toString()).getPlayerLevel())));
		
		Score leer2 = objective.getScore("  ");
		
		String xpString = Double.toString(Main.playerdataHandler.spielerDaten.get(p.getUniqueId().toString()).getXp());
		String xpToNext = Integer.toString(Main.playerdataHandler.spielerDaten.get(p.getUniqueId().toString()).getXpToNext());
		
		Score xp = objective.getScore(Bukkit.getOfflinePlayer("�6XP:"));
		Score xpValue = objective.getScore(Bukkit.getOfflinePlayer(xpString + "/" + xpToNext));
		
		Score trenner2 = objective.getScore(Bukkit.getOfflinePlayer("�7=============="));
		Score website = objective.getScore(Bukkit.getOfflinePlayer("darragesh-mc.de"));
		
		//Set values to Board
		name.setScore(13);
		nameValue.setScore(12);
		leer1.setScore(11);
		
		gold.setScore(10);
		goldValue.setScore(9);
		
		trenner.setScore(8);
		
		level.setScore(7);
		levelValue.setScore(6);
		leer2.setScore(5);
		
		xp.setScore(4);
		xpValue.setScore(3);
		
		trenner2.setScore(2);
		website.setScore(1);

		p.setScoreboard(board);
	}
	
	@SuppressWarnings("deprecation")
	public static void updateGold(Player p, String oldGold, String newGold){
		Scoreboard scoreboard = p.getScoreboard();
		Objective objective = scoreboard.getObjective("stats");
		
		Score goldNew = objective.getScore(Bukkit.getOfflinePlayer(newGold));
		scoreboard.resetScores(Bukkit.getOfflinePlayer(oldGold));
		goldNew.setScore(9);
	}
	
	@SuppressWarnings("deprecation")
	public static void updateXP(Player p, String oldXP, String newXP){
		Scoreboard scoreboard = p.getScoreboard();
		Objective objective = scoreboard.getObjective("stats");
		
		String xpStringOld = oldXP;
		String xpToNextOld = Integer.toString(Main.playerdataHandler.spielerDaten.get(p.getUniqueId().toString()).getXpToNext());
		scoreboard.resetScores(Bukkit.getOfflinePlayer(xpStringOld + "/" + xpToNextOld));
		
		String xpString = newXP;
		String xpToNext = Integer.toString(Main.playerdataHandler.spielerDaten.get(p.getUniqueId().toString()).getXpToNext());

		Score xpNew = objective.getScore(Bukkit.getOfflinePlayer(xpString + "/" + xpToNext));
		xpNew.setScore(3);
	}
	
	@SuppressWarnings("deprecation")
	public static void updateLeve(Player p, String oldLvl, String newLvl, String oldXpNeed, String newXpNeed, String oldXP){
		Scoreboard scoreboard = p.getScoreboard();
		Objective objective = scoreboard.getObjective("stats");
		
		String xpStringOld = oldXP;
		String xpToNextOld = oldXpNeed;
		scoreboard.resetScores(Bukkit.getOfflinePlayer(xpStringOld + "/" + xpToNextOld));
		
		Score xpNew = objective.getScore(Bukkit.getOfflinePlayer("0/" + newXpNeed));
		xpNew.setScore(3);
		
		scoreboard.resetScores(Bukkit.getOfflinePlayer(oldLvl));
		Score lvlNew = objective.getScore(Bukkit.getOfflinePlayer(newLvl));
		lvlNew.setScore(6);
	}
	
//	@SuppressWarnings("deprecation")
//	public static void updateScoreboard(Player p){
//		Bukkit.broadcastMessage(p.toString());
//		
//		Scoreboard scoarboard = p.getScoreboard();
//		Objective objective = scoarboard.getObjective("stats");
//		
//		Score name = objective.getScore(Bukkit.getOfflinePlayer("�6Name:"));
//		Score nameValue = objective.getScore(Bukkit.getOfflinePlayer(p.getName()));
//		
//		Score leer1 = objective.getScore(" ");
//		
//		Score gold = objective.getScore(Bukkit.getOfflinePlayer("�6Gold:"));
//		Score goldValue = objective.getScore(Bukkit.getOfflinePlayer(Long.toString(moneyUtil.getGold(p.getUniqueId().toString()))));
//		
//		Score trenner = objective.getScore(Bukkit.getOfflinePlayer("�7============"));
//		
//		Score level = objective.getScore(Bukkit.getOfflinePlayer("�6Level:"));
//		Score levelValue = objective.getScore(Bukkit.getOfflinePlayer(Integer.toString(Main.playerdataHandler.spielerDaten.get(p.getUniqueId().toString()).getPlayerLevel())));
//		
//		Score leer2 = objective.getScore("  ");
//		
//		String xpString = Double.toString(Main.playerdataHandler.spielerDaten.get(p.getUniqueId().toString()).getXp());
//		String xpToNext = Integer.toString(Main.playerdataHandler.spielerDaten.get(p.getUniqueId().toString()).getXpToNext());
//		
//		Score xp = objective.getScore(Bukkit.getOfflinePlayer("�6XP:"));
//		Score xpValue = objective.getScore(Bukkit.getOfflinePlayer(xpString + "/" + xpToNext));
//		
//		Score trenner2 = objective.getScore(Bukkit.getOfflinePlayer("�7=============="));
//		Score website = objective.getScore(Bukkit.getOfflinePlayer("darragesh-mc.de"));
//		
//		//Set values to Board
//		name.setScore(13);
//		nameValue.setScore(12);
//		leer1.setScore(11);
//		
//		gold.setScore(10);
//		goldValue.setScore(9);
//		
//		trenner.setScore(8);
//		
//		level.setScore(7);
//		levelValue.setScore(6);
//		leer2.setScore(5);
//		
//		xp.setScore(4);
//		xpValue.setScore(3);
//		
//		trenner2.setScore(2);
//		website.setScore(1);
//	}
	
}
