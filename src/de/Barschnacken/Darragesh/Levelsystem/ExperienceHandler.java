package de.Barschnacken.Darragesh.Levelsystem;

import java.util.ArrayList;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;

public class ExperienceHandler implements Listener {

	ArrayList<EntityType> mobs = levelUtil.loadMobList();
	ArrayList<EntityType> animals = levelUtil.loadAnimalList();
	
	@EventHandler
	public void onKillMob(EntityDeathEvent e){
		Entity killed = e.getEntity();
		
		if(killed.getLastDamageCause() instanceof EntityDamageByEntityEvent){
			EntityDamageByEntityEvent damageEvent = (EntityDamageByEntityEvent) killed.getLastDamageCause();
			
			if(damageEvent.getDamager() instanceof Player){
				Player damagePlayer = (Player) damageEvent.getDamager();
				
				//Mob wurde get�tet
				if(mobs.contains(killed.getType())){
					levelUtil.addExperience(damagePlayer.getUniqueId().toString(), 1, damagePlayer);
				}
				
				//Tier wurde get�tet
				if(animals.contains(killed.getType())){
					levelUtil.addExperience(damagePlayer.getUniqueId().toString(), 0.5, damagePlayer);
				}
			}
		}
	}
	
}
