package de.Barschnacken.Darragesh.ServerUtils;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;

import de.Barschnacken.Darragesh.Main;

public class serverlistEvent implements Listener {
	
	private Main plugin;

	public serverlistEvent(Main plugin) {
		this.plugin = plugin;
	}



	@EventHandler
	public void onPing(ServerListPingEvent e){
		e.setMotd("�2Darragesh �b" + plugin.getDescription().getVersion() + "\n" + "�cWir befinden uns im Aufbau!");
		
		int playerCount = Bukkit.getOnlinePlayers().size();
		
		if(playerCount < 10){
			e.setMaxPlayers(10);
		} else if(playerCount < 20) {
			e.setMaxPlayers(20);
		} else if(playerCount < 30) {
			e.setMaxPlayers(30);
		} else if(playerCount < 40) {
			e.setMaxPlayers(40);
		} else {
			e.setMaxPlayers(50);
		}
	}
	
}
