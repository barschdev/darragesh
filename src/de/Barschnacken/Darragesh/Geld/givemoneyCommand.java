package de.Barschnacken.Darragesh.Geld;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class givemoneyCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
		
		if(!cs.hasPermission("darragesh.admin.givemoney")){
			cs.sendMessage("�8[�7Darragesh�8]�c Du hast nicht die n�tige Berechtigung!");
			return true;
		}
		
		if(args.length < 2){
			cs.sendMessage("�8[�7Darragesh�8]�c Benutze /givemoney <Spieler> <Gold>");
			return true;
		}
		
		Player targetPlayer = Bukkit.getPlayer(args[0]);
		
		if(targetPlayer == null){
			cs.sendMessage("�8[�7Darragesh�8]�c Der angegebene Spieler ist nicht online.");
			return true;
		}
		
		moneyUtil.addMoney(targetPlayer.getUniqueId().toString(), Long.parseLong(args[1]), targetPlayer);

		//scoreBoardStats.updateScoreboard(targetPlayer);
		
		cs.sendMessage("�8[�7Darragesh�8]�2 Du hast �6" + targetPlayer.getDisplayName() + "�6 " + args[1] + " Gold �2gegeben.");
		return true;
	}

}
