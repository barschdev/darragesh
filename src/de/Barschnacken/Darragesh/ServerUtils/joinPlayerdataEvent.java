package de.Barschnacken.Darragesh.ServerUtils;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import de.Barschnacken.Darragesh.Main;
import de.Barschnacken.Darragesh.Database.Playerdata;
import de.Barschnacken.Darragesh.Database.scoreBoardStats;

public class joinPlayerdataEvent implements Listener {

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		Player player = e.getPlayer();
		
		if(!Main.playerdataHandler.spielerDaten.containsKey(player.getUniqueId().toString())){
			Playerdata newData = new Playerdata(player.getName(), 10, 0, 1, 0, 10);
			
			Main.playerdataHandler.spielerDaten.put(player.getUniqueId().toString(), newData);
			
			player.sendTitle("�2Willkommen auf Darragesh!", "�6Da du zum ersten mal hier bist, wurden neue Spielerdaten f�r dich angelegt.");
			scoreBoardStats.setupScoreboard(player);
		} else {
			player.sendTitle("�2Willkommen zur�ck!", "�6Deine Spielerdaten wurden erfolgreich geladen.");
		}
	}
	
}
