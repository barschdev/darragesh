package de.Barschnacken.Darragesh.Levelsystem;

import java.util.ArrayList;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import de.Barschnacken.Darragesh.Main;
import de.Barschnacken.Darragesh.Database.Playerdata;
import de.Barschnacken.Darragesh.Database.scoreBoardStats;

public class levelUtil {

	public static void levelUpPlayer(String uuid, Player scoreboardPlayer, String oldXP){
		Playerdata targetData = Main.playerdataHandler.spielerDaten.get(uuid);
		
		if(targetData.getPlayerLevel() >= 60){
			return;
		}
		
		String oldXpNeed = Integer.toString(targetData.getXpToNext());
		String oldLvl = Integer.toString(targetData.getPlayerLevel());
		targetData.setPlayerLevel(targetData.getPlayerLevel() + 1);
		String newLvl = Integer.toString(targetData.getPlayerLevel());
		targetData.setXp(0);
		
		int newXpNeeded = targetData.getPlayerLevel() * 10;
		targetData.setXpToNext(newXpNeeded);
		
		scoreBoardStats.updateLeve(scoreboardPlayer, oldLvl, newLvl, oldXpNeed, Integer.toString(newXpNeeded), oldXP);
	}
	
	public static void addExperience(String uuid, double amount, Player scoreboardPlayer){
		Playerdata targetData = Main.playerdataHandler.spielerDaten.get(uuid);
		
		String oldXP = Double.toString(targetData.getXp());
		targetData.setXp(targetData.getXp() + amount);
		String newXP = Double.toString(targetData.getXp());
		
		
		if(targetData.getXp() == targetData.getXpToNext()){
			levelUpPlayer(uuid, scoreboardPlayer, oldXP);
		} else {
			scoreBoardStats.updateXP(scoreboardPlayer, oldXP, newXP);;
		}
	}
	
	//Liste aller Entitys die als Monster gez�hlt werden sollen
	public static ArrayList<EntityType> loadMobList(){
		ArrayList<EntityType> hostileMobs = new ArrayList<EntityType>();
		
		hostileMobs.add(EntityType.BLAZE);
		hostileMobs.add(EntityType.CAVE_SPIDER);
		hostileMobs.add(EntityType.CREEPER);
		hostileMobs.add(EntityType.ENDER_DRAGON);
		hostileMobs.add(EntityType.ENDERMAN);
		hostileMobs.add(EntityType.ENDERMITE);
		hostileMobs.add(EntityType.GHAST);
		hostileMobs.add(EntityType.GIANT);
		hostileMobs.add(EntityType.GUARDIAN);
		hostileMobs.add(EntityType.MAGMA_CUBE);
		hostileMobs.add(EntityType.PIG_ZOMBIE);
		hostileMobs.add(EntityType.SILVERFISH);
		hostileMobs.add(EntityType.SKELETON);
		hostileMobs.add(EntityType.SLIME);
		hostileMobs.add(EntityType.SPIDER);
		hostileMobs.add(EntityType.WITCH);
		hostileMobs.add(EntityType.WITHER);
		hostileMobs.add(EntityType.ZOMBIE);
		
		return hostileMobs;
	}
	
	//Liste aller Entitys die als Tier gez�hlt werden sollen
	public static ArrayList<EntityType> loadAnimalList(){
		ArrayList<EntityType> animals = new ArrayList<EntityType>();
		
		animals.add(EntityType.BAT);
		animals.add(EntityType.CHICKEN);
		animals.add(EntityType.COW);
		animals.add(EntityType.HORSE);
		animals.add(EntityType.MUSHROOM_COW);
		animals.add(EntityType.OCELOT);
		animals.add(EntityType.PIG);
		animals.add(EntityType.RABBIT);
		animals.add(EntityType.SHEEP);
		animals.add(EntityType.SNOWMAN);
		animals.add(EntityType.SQUID);
		animals.add(EntityType.VILLAGER);
		animals.add(EntityType.WOLF);
		
		return animals;
	}
}
