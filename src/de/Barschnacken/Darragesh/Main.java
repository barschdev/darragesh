package de.Barschnacken.Darragesh;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import de.Barschnacken.Darragesh.Database.PlayerdataHandler;
import de.Barschnacken.Darragesh.Database.scoreBoardStats;
import de.Barschnacken.Darragesh.Geld.givemoneyCommand;
import de.Barschnacken.Darragesh.Geld.payCommand;
import de.Barschnacken.Darragesh.Levelsystem.ExperienceHandler;
import de.Barschnacken.Darragesh.Levelsystem.setLevelCommand;
import de.Barschnacken.Darragesh.ServerUtils.antilagRunnable;
import de.Barschnacken.Darragesh.ServerUtils.autosaveRunnable;
import de.Barschnacken.Darragesh.ServerUtils.joinPlayerdataEvent;
import de.Barschnacken.Darragesh.ServerUtils.serverlistEvent;
import de.Barschnacken.Darragesh.ServerUtils.warnantilagRunnable;

public class Main extends JavaPlugin {

	private static Main plugin;
	
	//Tasks
	private int autosaveTask;
	private int warnclearlagTask;
	private int clearlagTask;
	
	//Datenbankhandler-Klassen
	public static PlayerdataHandler playerdataHandler;
	
	//Plugin f�r andere Klassen freigeben
	public static Main getMain(){
		return Main.plugin;
	}
	
	@Override
	public void onEnable(){
		Main.plugin = this;
		PluginManager pm = Bukkit.getPluginManager();
		
		//Datenbankhandler-Klassen laden
		playerdataHandler = new PlayerdataHandler();
		
		//ServerUtil Tasks laden
		this.autosaveTask = Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new autosaveRunnable(), 600*20L, 600*20L);
		this.warnclearlagTask = Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new warnantilagRunnable(), 240*20L, 300*20L);
		this.clearlagTask = Bukkit.getScheduler().scheduleSyncRepeatingTask(this, new antilagRunnable(),300*20L, 300*20L);
		
		//Events laden
		pm.registerEvents(new serverlistEvent(this), this);
		pm.registerEvents(new scoreBoardStats(this), this);
		pm.registerEvents(new joinPlayerdataEvent(), this);
		pm.registerEvents(new ExperienceHandler(), this);
		
		//Spielerbefehle laden
		this.getCommand("pay").setExecutor(new payCommand());
		
		//Adminbefehle laden
		this.getCommand("givemoney").setExecutor(new givemoneyCommand());
		this.getCommand("setlevel").setExecutor(new setLevelCommand());
		
		System.out.println(ChatColor.GREEN + "########################################");
		System.out.println(ChatColor.GREEN + "Darragesh Script Version " + this.getDescription().getVersion() + " geladen.");
		System.out.println(ChatColor.GREEN + "########################################");
	}
	
	@Override
	public void onDisable(){
		//Tasks beenden
		Bukkit.getScheduler().cancelTask(autosaveTask);
		Bukkit.getScheduler().cancelTask(warnclearlagTask);
		Bukkit.getScheduler().cancelTask(clearlagTask);
		
		
		//Datenbankhandler speichern lassen
		playerdataHandler.savePlayerdata();
		
		System.out.println(ChatColor.GREEN + "########################################");
		System.out.println(ChatColor.GREEN + "Darragesh Script Version " + this.getDescription().getVersion() + " beendet.");
		System.out.println(ChatColor.GREEN + "########################################");
	}
	
}