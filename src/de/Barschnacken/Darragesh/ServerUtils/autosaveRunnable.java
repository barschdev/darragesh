package de.Barschnacken.Darragesh.ServerUtils;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Player;

public class autosaveRunnable implements Runnable {

	@Override
	public void run() {
		
		Bukkit.broadcastMessage("§8[§7Darragesh§8] Spieler-/Weltdaten werden gesichert..");
		
		for(Player p : Bukkit.getOnlinePlayers()){
			p.saveData();
		}
		
		for(World w : Bukkit.getWorlds()){
			w.save();
		}
		
		Bukkit.broadcastMessage("§8[§7Darragesh§8] Sicherung abgeschlossen!");
		
	}

}
