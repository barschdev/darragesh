package de.Barschnacken.Darragesh.Levelsystem;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import de.Barschnacken.Darragesh.Main;

public class setLevelCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender cs, Command cmd, String label,String[] args) {

		if(!cs.hasPermission("darragesh.admin.setlevel")){
			cs.sendMessage("�8[�7Darragesh�8]�c Du hast nicht die n�tige Berechtigung!");
			return true;
		}
		
		if(args.length < 2){
			cs.sendMessage("�8[�7Darragesh�8]�c Benutze /setlevel <Spieler> <Level>");
			return true;
		}
		
		Player targetPlayer = Bukkit.getPlayer(args[0]);
		
		if(targetPlayer == null){
			cs.sendMessage("�8[�7Darragesh�8]�c Der angegebene Spieler ist nicht online.");
			return true;
		}
		
		Main.playerdataHandler.spielerDaten.get(targetPlayer.getUniqueId().toString()).setPlayerLevel(Integer.parseInt(args[1]));
		
		//scoreBoardStats.updateLeve(targetPlayer, oldLvl, newLvl, oldXpNeed, );
		
		cs.sendMessage("�8[�7Darragesh�8]�2 Du hast das Level von �6" + targetPlayer.getDisplayName() + "�2 auf �6" + args[1] + "�2 gesetzt.");
		return true;
	}

}
