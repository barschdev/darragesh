package de.Barschnacken.Darragesh.Database;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public class PlayerdataHandler {

	public File player_file = new File("plugins/Darragesh", "playerdata.yml");
	public FileConfiguration playerdata_cfg = YamlConfiguration.loadConfiguration(player_file);
	
	public HashMap<String, Playerdata> spielerDaten = new HashMap<String, Playerdata>();
	
	public PlayerdataHandler() {
		loadPlayerdata();
	}
	
	public void loadPlayerdata(){
		for(String uuid : this.playerdata_cfg.getConfigurationSection("").getKeys(false)){
			String playerName = this.playerdata_cfg.getString(uuid + ".name");
			
			int level = this.playerdata_cfg.getInt(uuid + ".level");
			
			long goldAmount = this.playerdata_cfg.getLong(uuid + ".gold");
			int silberAmount = this.playerdata_cfg.getInt(uuid + ".silber");
			
			double xp = this.playerdata_cfg.getDouble(uuid + ".erfahrung");
			int xpToNext = this.playerdata_cfg.getInt(uuid + ".erfahrunNoetig");
			
			this.spielerDaten.put(uuid, new Playerdata(playerName, goldAmount, silberAmount, level, xp, xpToNext));
		}
	}
	
	public void savePlayerdata(){
		for(String uuid : this.spielerDaten.keySet()){
			this.playerdata_cfg.set(uuid, this.spielerDaten.get(uuid).serialize());
		}
		
		try {
			this.playerdata_cfg.save(player_file);
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
}
