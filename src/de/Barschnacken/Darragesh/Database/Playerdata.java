package de.Barschnacken.Darragesh.Database;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.serialization.ConfigurationSerializable;

public class Playerdata implements ConfigurationSerializable {
	
	private String player_name;
	private long goldAmount;
	private int silberAmount;
	private int playerLevel;
	private double xp;
	private int xpToNext;
	
	public Playerdata(String player_name, long goldAmount, int silberAmount, int playerLevel, double xp, int xpToNext) {
		this.player_name = player_name;
		this.goldAmount = goldAmount;
		this.silberAmount = silberAmount;
		this.playerLevel = playerLevel;
		this.xp = xp;
		this.xpToNext = xpToNext;
	}
	
	@Override
	public Map<String, Object> serialize(){
		Map<String, Object> o = new HashMap<String, Object>();
		
		o.put("name", this.player_name);
		o.put("gold", this.goldAmount);
		o.put("silber", this.silberAmount);
		o.put("level", this.playerLevel);
		o.put("erfahrung", this.xp);
		o.put("erfahrunNoetig", this.xpToNext);
		
		return o;
	}

	//Getter & Setter
	public String getPlayer_name() {
		return player_name;
	}

	public void setPlayer_name(String player_name) {
		this.player_name = player_name;
	}

	public long getGoldAmount() {
		return goldAmount;
	}

	public void setGoldAmount(long goldAmount) {
		this.goldAmount = goldAmount;
	}

	public int getSilberAmount() {
		return silberAmount;
	}

	public void setSilberAmount(int silberAmount) {
		this.silberAmount = silberAmount;
	}
	
	public int getPlayerLevel() {
		return playerLevel;
	}
	
	public void setPlayerLevel(int playerLevel) {
		this.playerLevel = playerLevel;
	}

	public double getXp() {
		return xp;
	}

	public void setXp(double xp) {
		this.xp = xp;
	}

	public int getXpToNext() {
		return xpToNext;
	}

	public void setXpToNext(int xpToNext) {
		this.xpToNext = xpToNext;
	}
}
